# frozen_string_literal: true

require_relative '../../../lib/constants/labels'
require_relative '../../triage/processor'

module Triage
  class AddThreatInsightsGroupLabel < Processor
    react_to 'issue.open', 'issue.update'

    def applicable?
      event.from_part_of_product_project? &&
        !has_group_label? &&
        team_label_added?
    end

    def process
      add_group_label
    end

    def documentation
      <<~TEXT
      Add the Threat Insights group label to an issue that receives any of the Threat Insights
      team labels.

      The scoped labels for the Threat Insights team are only relevant within this group. As
      discussed in the 15.4 retrospective:

      https://gitlab.com/gl-retrospectives/govern-sub-department/threat-insights/-/issues/25#note_1102784822
      TEXT
    end

    private

    def has_group_label?
      event.label_names.include?(Labels::THREAT_INSIGHTS_GROUP_LABEL)
    end

    def team_label_added?
      Labels::THREAT_INSIGHTS_TEAM_LABELS.any? { |label| event.added_label_names.include?(label) }
    end

    def add_group_label
      body = "/label #{Labels::THREAT_INSIGHTS_GROUP_LABEL}"
      add_comment(body, append_source_link: false)
    end
  end
end
