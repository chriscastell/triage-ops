# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/priority_mapping_helper'

RSpec.describe PriorityMappingHelper do
  let(:resource_klass) do
    Struct.new(:labels) do
      include PriorityMappingHelper
    end
  end

  let(:label_klass) do
    Struct.new(:name)
  end

  let(:labels) { [] }

  subject { resource_klass.new(labels) }

  describe '#set_priority_for_bug' do
    context 'when severity label exists' do
      let(:labels) { [label_klass.new('severity::1')] }

      context 'when priority label applied' do
        context 'when correct priority label already' do
          before do
            labels << label_klass.new('priority::1')
          end

          it 'will not set priority' do
            expect(subject.set_priority_for_bug).to eq('')
          end
        end

        context 'when mismatched priority label' do
          before do
            labels << label_klass.new('priority::2')
          end

          it 'will not override priority' do
            expect(subject.set_priority_for_bug).to eq('')
          end
        end
      end

      context 'when priority label not applied' do
        it 'will set correct priority' do
          expect(subject.set_priority_for_bug).to eq('/label ~"priority::1"')
        end
      end

      context 'when merge requests' do
        before do
          labels << label_klass.new(PriorityMappingHelper::MERGE_REQUEST_LABEL)
          labels << label_klass.new(PriorityMappingHelper::UX_LABEL)
        end

        it 'sets p1 for s1' do
          expect(subject.set_priority_for_bug).to eq('/label ~"priority::1"')
        end

        it 'sets p1 for s2' do
          labels.delete(label_klass.new('severity::1'))
          labels << label_klass.new('severity::2')

          expect(subject.set_priority_for_bug).to eq('/label ~"priority::1"')
        end

        it 'overrides p2 for s1' do
          labels << label_klass.new('priority::2')

          expect(subject.set_priority_for_bug).to include('Prioritization has been automatically updated')
          expect(subject.set_priority_for_bug).to include('/label ~"priority::1"')
        end

        it 'bumps set priority for s4' do
          labels.delete(label_klass.new('severity::1'))
          labels << label_klass.new('severity::4')
          labels << label_klass.new('priority::4')

          expect(subject.set_priority_for_bug).to include('Prioritization has been automatically updated')
          expect(subject.set_priority_for_bug).to include('/label ~"priority::3"')
        end

        it 'does not override greater priority than default' do
          labels.delete(label_klass.new('severity::1'))
          labels << label_klass.new('severity::4')
          labels << label_klass.new('priority::2')

          expect(subject.set_priority_for_bug).to include('')
        end

        it 'does not comment for existing matching priority' do
          labels.delete(label_klass.new('severity::1'))
          labels << label_klass.new('severity::3')
          labels << label_klass.new('priority::2')

          expect(subject.set_priority_for_bug).to include('')
        end
      end

      context 'when out of scope issues' do
        it 'does not affect s3 bug with no priority' do
          labels.delete(label_klass.new('severity::1'))
          labels << label_klass.new('severity::4')

          expect(subject.set_priority_for_bug).to eq('')
        end
      end
    end

    context 'when severity label does not exist' do
      it 'will not set priority' do
        expect(subject.set_priority_for_bug).to eq('')
      end
    end

    context 'when out of scope severity label exists' do
      let(:labels) { [label_klass.new('severity::4')] }

      it 'will not set priority' do
        expect(subject.set_priority_for_bug).to eq('')
      end
    end
  end

  describe '#has_severity?' do
    context 'when severity label exists' do
      let(:labels) { [label_klass.new('severity::1')] }

      it { expect(subject).to have_severity }
    end

    context 'when severity label does not exist' do
      it { expect(subject).not_to have_severity }
    end
  end

  describe '#has_no_severity?' do
    context 'when severity label exists' do
      let(:labels) { [label_klass.new('severity::1')] }

      it { expect(subject).not_to have_no_severity }
    end

    context 'when severity label does not exist' do
      it { expect(subject).to have_no_severity }
    end
  end

  describe '#has_priority?' do
    context 'when priority label exists' do
      let(:labels) { [label_klass.new('priority::1')] }

      it { expect(subject).to have_priority }
    end

    context 'when priority label does not exist' do
      it { expect(subject).not_to have_priority }
    end
  end

  describe '#has_no_priority?' do
    context 'when priority label exists' do
      let(:labels) { [label_klass.new('priority::1')] }

      it { expect(subject).not_to have_no_priority }
    end

    context 'when priority label does not exist' do
      it { expect(subject).to have_no_priority }
    end
  end
end
