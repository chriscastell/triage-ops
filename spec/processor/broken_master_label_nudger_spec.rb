# frozen_string_literal: true

require 'spec_helper'
require_relative '../../triage/processor/broken_master_label_nudger'
require_relative '../../triage/job/broken_master_label_nudger_job'
require_relative '../../triage/triage/event'

RSpec.describe Triage::BrokenMasterLabelNudger do
  include_context 'with event', 'Triage::IssueEvent' do
    let(:is_master_broken_incident_project) { true }
    let(:gitlab_bot_event_actor) { false }
    let(:event_attrs) do
      { from_master_broken_incidents_project?: is_master_broken_incident_project,
        gitlab_bot_event_actor?: gitlab_bot_event_actor,
        label_names: label_names }
    end

    let(:label_names) { ['master-broken::undetermined'] }
  end

  subject { described_class.new(event) }

  before do
    stub_api_request(
      path: "/projects/#{project_id}/issues/#{iid}/notes",
      query: { per_page: 100 },
      response_body: []
    )
  end

  include_examples 'registers listeners', ['issue.close']

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    let(:validator) { subject.__send__(:validator) }

    context 'when event is not from master-broken-incident project' do
      let(:is_master_broken_incident_project) { false }

      include_examples 'event is not applicable'
    end

    context 'when event actor is gitlab-bot' do
      let(:gitlab_bot_event_actor) { true }

      include_examples 'event is not applicable'
    end

    context 'when incident does not need a root cause label or a flaky reason label' do
      before do
        allow(validator).to receive(:need_root_cause_label?).and_return(false)
        allow(validator).to receive(:need_flaky_reason_label?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when incident needs a root cause label, but not a flaky test label' do
      before do
        allow(validator).to receive(:need_root_cause_label?).and_return(true)
        allow(validator).to receive(:need_flaky_reason_label?).and_return(false)
      end

      include_examples 'event is applicable'
    end

    context 'when incident does not need a root cause label, but but needs a flaky test label' do
      before do
        allow(validator).to receive(:need_root_cause_label?).and_return(false)
        allow(validator).to receive(:need_flaky_reason_label?).and_return(true)
      end

      include_examples 'event is applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'schedules a BrokenMasterLabelNudgerJob 5 minutes later' do
      expect(Triage::BrokenMasterLabelNudgerJob).to receive(:perform_in).with(described_class::FIVE_MINUTES, event)
      subject.process
    end
  end
end
