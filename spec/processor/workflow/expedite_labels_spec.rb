# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/workflow/expedite_labels'

RSpec.describe Triage::Workflow::ExpediteLabels do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        from_gitlab_org?: true,
        title: mr_title
      }
    end

    let(:mr_title) { 'This is an non-revert MR title' }
    let(:label_names) { [Labels::MASTER_BROKEN_LABEL] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open', 'merge_request.update']

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when no labels are set' do
      let(:label_names) { [] }

      include_examples 'event is not applicable'
    end

    context 'when "pipeline:expedite" is set' do
      let(:label_names) { [Labels::PIPELINE_EXPEDITE_LABEL] }

      include_examples 'event is applicable'
    end

    context 'when we do not have a revert MR and "pipeline:expedite" is set' do
      let(:mr_title) { 'Not a revert MR title' }
      let(:label_names) { [Labels::PIPELINE_EXPEDITE_LABEL] }

      include_examples 'event is applicable'
    end

    context 'when "master:broken" is set and "pipeline:expedite" is set' do
      let(:label_names) { [Labels::MASTER_BROKEN_LABEL, Labels::PIPELINE_EXPEDITE_LABEL] }

      include_examples 'event is not applicable'
    end

    context 'when "master:foss-broken" is set and "pipeline:expedite" is set' do
      let(:label_names) { [Labels::MASTER_FOSS_BROKEN_LABEL, Labels::PIPELINE_EXPEDITE_LABEL] }

      include_examples 'event is not applicable'
    end

    context 'when we have a revert MR and "pipeline:expedite" is set' do
      let(:mr_title) { 'Revert MR title' }
      let(:label_names) { [Labels::PIPELINE_EXPEDITE_LABEL] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    shared_examples '"pipeline:expedite" label is added' do
      it 'posts a comment' do
        body = <<~MARKDOWN.chomp
          /label ~"#{Labels::PIPELINE_EXPEDITE_LABEL}"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    shared_examples '"pipeline:expedite" label is not added' do
      it 'does not post a comment' do
        expect_no_request do
          subject.process
        end
      end
    end

    context 'when "master:broken" is set' do
      let(:label_names) { [Labels::MASTER_BROKEN_LABEL] }

      context 'when "pipeline:expedite" is not set' do
        it_behaves_like '"pipeline:expedite" label is added'
      end
    end

    context 'when "master:foss-broken" is set' do
      let(:label_names) { [Labels::MASTER_FOSS_BROKEN_LABEL] }

      context 'when "pipeline:expedite" is not set' do
        it_behaves_like '"pipeline:expedite" label is added'
      end
    end

    context 'when it is a revert MR' do
      let(:mr_title) { 'revert MR title' }

      context 'when "pipeline:expedite" is not set' do
        let(:label_names) { [] }

        it_behaves_like '"pipeline:expedite" label is not added'
      end
    end

    context 'when "master:broken" is not set' do
      context 'when "pipeline:expedite" is set' do
        let(:label_names) { [Labels::PIPELINE_EXPEDITE_LABEL] }

        it 'posts a comment' do
          body = <<~MARKDOWN.chomp
            Setting ~"#{Labels::PIPELINE_EXPEDITE_LABEL}" without ~"#{Labels::MASTER_BROKEN_LABEL}" or ~"#{Labels::MASTER_FOSS_BROKEN_LABEL}" or outside a revert MR is forbidden!
            /unlabel ~"#{Labels::PIPELINE_EXPEDITE_LABEL}"
          MARKDOWN

          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end
      end
    end

    context 'when it is NOT a revert MR' do
      let(:mr_title) { 'Not a revert MR title' }

      context 'when "pipeline:expedite" is set' do
        let(:label_names) { [Labels::PIPELINE_EXPEDITE_LABEL] }

        it 'posts a comment' do
          body = <<~MARKDOWN.chomp
            Setting ~"#{Labels::PIPELINE_EXPEDITE_LABEL}" without ~"#{Labels::MASTER_BROKEN_LABEL}" or ~"#{Labels::MASTER_FOSS_BROKEN_LABEL}" or outside a revert MR is forbidden!
            /unlabel ~"#{Labels::PIPELINE_EXPEDITE_LABEL}"
          MARKDOWN

          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end
      end
    end
  end
end
