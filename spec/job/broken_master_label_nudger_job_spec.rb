# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/job/broken_master_label_nudger_job'
require_relative '../../triage/triage/event'

RSpec.describe Triage::BrokenMasterLabelNudgerJob do
  include_context 'with event', 'Triage::IssueEvent' do
    let(:event_attrs) do
      { project_id: Triage::Event::MASTER_BROKEN_INCIDENT_PROJECT_ID,
        event_actor_username: 'username',
        label_names: label_names }
    end
  end

  let(:label_names)              { ['master-broken::unknown'] }
  let(:issue_notes_api_path)     { "/projects/#{project_id}/issues/#{iid}/notes" }
  let(:incident_label_validator) { subject.__send__(:validator) }

  let(:missing_root_cause_label_comment_request_body) do
    <<~MARKDOWN.chomp
      <!-- triage-serverless BrokenMasterLabelNudgerJob--RootCauseLabelMissing -->
      :wave: @username, #{described_class::ROOT_CAUSE_LABEL_MISSING_MESSAGE}
    MARKDOWN
  end

  let(:missing_flaky_reason_label_comment_request_body) do
    <<~MARKDOWN.chomp
      <!-- triage-serverless BrokenMasterLabelNudgerJob--FlakyReasonLabelMissing -->
      :wave: @username, #{described_class::FLAKY_REASON_LABEL_MISSING_MESSAGE}
    MARKDOWN
  end

  subject { described_class.new }

  describe '#perform' do
    context 'when incident does not require any label change' do
      before do
        allow(incident_label_validator).to receive(:need_root_cause_label?).and_return(false)
        allow(incident_label_validator).to receive(:need_flaky_reason_label?).and_return(false)
      end

      it 'does nothing' do
        expect_no_request(
          verb: :post,
          request_body: { body: missing_root_cause_label_comment_request_body },
          path: issue_notes_api_path,
          response_body: {}
        ) do
          subject.perform(event)
        end

        expect_no_request(
          verb: :post,
          request_body: { body: missing_root_cause_label_comment_request_body },
          path: issue_notes_api_path,
          response_body: {}
        ) do
          subject.perform(event)
        end
      end
    end

    context 'when incident needs a root cause label' do
      before do
        allow(incident_label_validator).to receive(:need_root_cause_label?).and_return(true)
        allow(incident_label_validator).to receive(:need_flaky_reason_label?).and_return(false)
      end

      it 'pings username in a nudge comment for adding root cause label' do
        expect_comment_request(event: event, body: missing_root_cause_label_comment_request_body) do
          subject.perform(event)
        end
      end
    end

    context 'when incident needs a flaky test reason label' do
      before do
        allow(incident_label_validator).to receive(:need_root_cause_label?).and_return(false)
        allow(incident_label_validator).to receive(:need_flaky_reason_label?).and_return(true)
      end

      it 'pings username in a nudge comment for adding flaky test reason label' do
        expect_api_request(
          verb: :post,
          request_body: { body: missing_flaky_reason_label_comment_request_body },
          path: issue_notes_api_path,
          response_body: {}
        ) do
          subject.perform(event)
        end
      end
    end
  end
end
